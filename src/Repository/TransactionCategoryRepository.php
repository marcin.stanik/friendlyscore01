<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\TransactionCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TransactionCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method TransactionCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method TransactionCategory[]    findAll()
 * @method TransactionCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @author Marcin Stanik <marcin.stanik@gmail.com>
 * @since 09.2020
 */
class TransactionCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TransactionCategory::class);
    }

    /**
     * @param string $name
     * @param bool $createIfNotExist
     * @return TransactionCategory|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function getTransactionCategoryByName(string $name, bool $createIfNotExist = false): ?TransactionCategory
    {
        static $transactionCategories = [];

        if (!array_key_exists($name, $transactionCategories)) {

            $transactionCategories[$name] = $this->findOneBy([
                'name' => $name,
            ]);

            if ($transactionCategories[$name] === null && $createIfNotExist === true) {
                $transactionCategory = new TransactionCategory();
                $transactionCategory->setName($name);

                $this->getEntityManager()->persist($transactionCategory);
                $this->getEntityManager()->flush();

                $transactionCategories[$name] = $transactionCategory;
            }
        }

        return $transactionCategories[$name];
    }

    /**
     * @param string $name
     * @param bool $createIfNotExist
     * @return int|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function getTransactionCategoryIdByName(string $name, bool $createIfNotExist = false): ?int
    {
        $transactionCategory = $this->getTransactionCategoryByName($name, $createIfNotExist);

        return $transactionCategory !== null
            ? $transactionCategory->getId()
            : null;
    }

}
