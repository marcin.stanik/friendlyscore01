<?php
declare(strict_types=1);

namespace App\FriendlyScore;

use App\Entity\Currency;
use App\Entity\TransactionCategory;
use App\Entity\TransactionInformationGroup;
use Doctrine\ORM\EntityManagerInterface;
use SplObserver;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class ImportTransactions
 * @package App\FriendlyScore
 * @author Marcin Stanik <marcin.stanik@gmail.com>
 * @since 09.20020
 */
class ImportTransactions
    implements \SplSubject
{
    /** @var string */
    const API_URL = 'https://fs-demo-bank.friendlyscore.com/transactions';

    /** @var string[] */
    const CATEGORIES = [
        'Category 1',
        'Category 2',
        'Category 3',
        'Category 4',
        'Category 5',
    ];

    /** @var \App\FriendlyScore\TransactionInformationGroup  */
    protected \App\FriendlyScore\TransactionInformationGroup $transactionInformationGroup;

    /** @var HttpClientInterface */
    protected HttpClientInterface $httpClient;

    /** @var EntityManagerInterface */
    protected EntityManagerInterface $em;

    /** @var SplObserver[] */
    private array $observers = [];

    /** @var string  */
    private string $status = '';

    /**
     * ImportTransactions constructor.
     * @param \App\FriendlyScore\TransactionInformationGroup $transactionInformationGroup
     * @param HttpClientInterface $httpClient
     * @param EntityManagerInterface $em
     */
    public function __construct(\App\FriendlyScore\TransactionInformationGroup $transactionInformationGroup, HttpClientInterface $httpClient, EntityManagerInterface $em)
    {
        $this->transactionInformationGroup = $transactionInformationGroup;
        $this->httpClient = $httpClient;
        $this->em = $em;
    }

    /**
     * @param SplObserver $observer
     * @return void
     */
    public function attach(SplObserver $observer): void
    {
        if (!in_array($observer, $this->observers)) {
            $this->observers[] = $observer;
        }
    }

    /**
     * @param SplObserver $observer
     * @return void
     */
    public function detach(SplObserver $observer): void
    {
        $key = array_search($observer, $this->observers, true);
        if ($key) {
            unset($this->observers[$key]);
        }
    }

    /**
     * @return void
     */
    public function notify() : void
    {
        foreach ($this->observers as $observer) {
            $observer->update($this);
        }
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function import(): void
    {
        $timeStart = microtime(true);
        $csvFilePath = 'var/file.csv';

        $this->setStatus('START');
        $this->setStatus('Fetching information about transactions from external API ...');

        $response = $this->httpClient->request(
            'GET',
            self::API_URL,
            [
                'headers' => [
                    'Accept' => 'application/json',
                ],
            ]
        );

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            throw new \Exception('Error during connection with external API!');
        } else if (($contentType = $response->getHeaders()['content-type'][0]) !== 'application/json') {
            throw new \Exception("Error - expecting application/json but $contentType givem!");
        }

        $csvFileHandle = fopen($csvFilePath, 'w');

        $this->setStatus('Filling information about transactions to CSV file ...');

        array_map(
            function ($transaction) use ($csvFileHandle) {

                $transactionInformationGroupId = null;
                if (!empty($transaction['TransactionInformation'])) {
                    $transactionInformationGroupName = $this->transactionInformationGroup->getTransactionGroupNameFromInformation($transaction['TransactionInformation']);

                    $transactionInformationGroupId = !empty($transactionInformationGroupName)
                        ? $this->em->getRepository(TransactionInformationGroup::class)
                            ->getTransactionInformationGroupIdByName($transactionInformationGroupName, true)
                        : null;
                }

                $transactionCategoryName = self::CATEGORIES[rand(0, count(self::CATEGORIES) - 1)];
                $transactionCategoryId = $this->em->getRepository(TransactionCategory::class)
                    ->getTransactionCategoryIdByName($transactionCategoryName, true);

                $currencyId = !empty($transaction['Amount']['Currency'])
                    ? $this->em->getRepository(Currency::class)
                        ->getCurrencyIdByName($transaction['Amount']['Currency'], true)
                    : null;

                fputcsv(
                    $csvFileHandle,
                    [
                        $transaction['TransactionId'],
                        $transactionInformationGroupId,
                        $transactionCategoryId,
                        $currencyId,
                        $transaction['BookingDateTime'],
                        $transaction['TransactionInformation'],
                        $transaction['Amount']['Amount'] ?? 0,
                    ],
                    ';'
                );
            },
            $response->toArray()['Data']['Transaction'] ?? []
        );

        fclose($csvFileHandle);

        $this->importToTransactionTable(realpath(__DIR__ . '/../../' . $csvFilePath));

        unlink($csvFilePath);

        $this->setStatus('END');
        $this->setStatus('Total execution time in seconds: ' . (microtime(true) - $timeStart));
    }

    /**
     * @param string $status
     */
    protected function setStatus(string $status): void
    {
        $this->status = $status;
        $this->notify();
    }

    /**
     * @param string $csvFilePath
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    private function importToTransactionTable(string $csvFilePath): void
    {
        $queries = [
            'Creating temporary table ...' => '
CREATE TEMPORARY TABLE IF NOT EXISTS "tmp_transaction" (
    "transaction_id" character varying(45) NOT NULL,
    "transaction_information_group_id" integer,
    "transaction_category_id" integer,
    "amount_currency_id" integer,
    "booking_date_time" timestamp without time zone,
    "transaction_information" character varying(500),
    "amount" numeric(20,8)
) WITH (oids = false);
            ',
            'Set index for temporary table ...' => 'CREATE INDEX  IF NOT EXISTS "tmp_transaction_transaction_id" ON "tmp_transaction" USING btree ("transaction_id");',
            '' => 'TRUNCATE tmp_transaction',
            'Copy/inserting records from CSV file to temporary table ...' => "COPY tmp_transaction FROM '$csvFilePath' DELIMITER ';' CSV HEADER",
            'Copy/inserting missing transactions to transaction table...' => '
INSERT INTO "public"."transaction"
(
    transaction_id,
    transaction_information_group_id,
    transaction_category_id,
    amount_currency_id,
    booking_date_time,
    transaction_information,
    amount
) 
SELECT 
    transaction_id,
    transaction_information_group_id,
    transaction_category_id,
    amount_currency_id,
    booking_date_time,
    transaction_information,
    amount
FROM  "tmp_transaction"
WHERE transaction_id NOT IN (
    SELECT transaction_id FROM "public"."transaction"
)
            '
        ];

        foreach ($queries as $status=>$query) {
            if (strlen($status) > 0) {
                $this->setStatus($status);
            }

            $stmt = $this->em->getConnection()->prepare($query);
            $stmt->execute();
        }
    }

}
