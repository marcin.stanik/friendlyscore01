<?php
declare(strict_types=1);

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Transaction as TransactionEntity;
use App\Entity\TransactionCategory as TransactionCategoryEntity;

/**
 * Class Transaction
 * @package App\Service
 * @author Marcin Stanik <marcin.stanik@gmail.com>
 * @since 09.2020
 */
class Transaction
{

    /** @var EntityManagerInterface */
    protected EntityManagerInterface $em;

    /**
     * Transaction constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @return mixed
     */
    public function getAllTransactionsArray()
    {
        return $this->em->getRepository(TransactionEntity::class)
            ->getArray();
    }

    /**
     * @param string $transactionId
     * @param string $categoryName
     * @return array
     */
    public function updateTransactionCategory(string $transactionId, string $categoryName): array
    {
        $updatedTransactionIds = [];

        $transaction = $this->em->getRepository(TransactionEntity::class)
            ->find($transactionId);

        if ($transaction instanceof TransactionEntity) {
            $updatedTransactionIds[] = $transaction->getTransactionId();

            $transactionCategory = $this->em->getRepository(TransactionCategoryEntity::class)
                ->getTransactionCategoryByName($categoryName, true);

            $transaction->setTransactionCategory($transactionCategory);
            $this->em->persist($transaction);

            $transactionInformationGroupId = $transaction->getTransactionInformationGroup() != null
                ? $transaction->getTransactionInformationGroup()->getId()
                : null;

            if ($transactionInformationGroupId > 0) {
                $transactions = $this->em->getRepository(TransactionEntity::class)
                    ->getTransactionsByTransactionInformationGroupId($transactionInformationGroupId, [$transaction->getTransactionId()]);

                foreach ($transactions as $_transaction) {
                    $updatedTransactionIds[] = $_transaction->getTransactionId();

                    $_transaction->setTransactionCategory($transactionCategory);
                    $this->em->persist($_transaction);
                }
            }

            $this->em->flush();
        }

        return count($updatedTransactionIds) > 0
            ? $this->em->getRepository(TransactionEntity::class)
                ->getArray($updatedTransactionIds)
            : [];
    }

    /**
     * @return void
     */
    public function deleteNewestTransactions(int $count): void
    {
        $this->em->getRepository(TransactionEntity::class)
            ->deleteNewest($count);
    }

    /**
     * @return void
     */
    public function deleteAllTransactions(): void
    {
        $this->em->getRepository(TransactionEntity::class)
            ->truncate();
    }

}
