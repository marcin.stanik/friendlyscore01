<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class AbstractApiController
 * @package App\Controller
 * @author Marcin Stanik <marcin.stanik@gmail.com>
 * @since 09.2020
 */
abstract class AbstractApiController extends AbstractController
{

}