<?php
declare(strict_types=1);

namespace App\FriendlyScore;

/**
 * Class TransactionInformationGroup
 * @package App\FriendlyScore
 * @author Marcin Stanik <marcin.stanik@gmail.com>
 * @since 09.20020
 */
class TransactionInformationGroup
{

    /** @var string[] */
    const TRANSACTION_GROUP_NAME_SEPARATORS = [
        ' ON ',
        'ADMIRAL INSURANCE ',
        'ASDA PETROL',
        'EE LIMITED  Q',
        'PURE GYM LTD ',
        'TEST COMPANY SALARY ',
    ];

    /**
     * @param string $transactionInformation
     * @return string
     */
    public function getTransactionGroupNameFromInformation(string $transactionInformation): string
    {
        $transactionGroupName = $transactionInformation;

        foreach (self::TRANSACTION_GROUP_NAME_SEPARATORS as $separator) {
            if (($tmp = stristr($transactionInformation, $separator, true)) !== false) {
                $transactionGroupName = $tmp . $separator;
                break;
            }
        }

        return $transactionGroupName;
    }

}