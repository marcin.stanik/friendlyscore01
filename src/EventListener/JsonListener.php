<?php
declare(strict_types=1);

namespace App\EventListener;

use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class JsonListener
 * @package App\EventListener
 * @author Marcin Stanik <marcin.stanik@gmail.com>
 * @since 09.2020
 */
class JsonListener
{

    /**
     * @param FilterControllerEvent $event
     */
    public function onKernelController(ControllerEvent $event)
    {
        $data = json_decode($event->getRequest()->getContent(), true);

        if ($data !== null) {
            $event->getRequest()->request->replace($data);
        }
    }

}

