<?php
declare(strict_types=1);

namespace App\Tests\Command;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Class ImportTransactionsCommandTest
 * @package App\Tests\Command
 * @author Marcin Stanik <marcin.stanik@gmail.com>
 * @since 09.2020
 */
class ImportTransactionsCommandTest extends KernelTestCase
{
    public function testExecute()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('app:import-transactions');
        $commandTester = new CommandTester($command);
        $commandTester->execute([]);

        $output = $commandTester->getDisplay();

        $this->assertEquals(Command::SUCCESS, $commandTester->getStatusCode());

        $this->assertStringContainsString('START', $output);
        $this->assertStringContainsString('Fetching information about transactions from external API ...', $output);
        $this->assertStringContainsString('Filling information about transactions to CSV file ...', $output);
        $this->assertStringContainsString('Creating temporary table ...', $output);
        $this->assertStringContainsString('Set index for temporary table ...', $output);
        $this->assertStringContainsString('Copy/inserting records from CSV file to temporary table ...', $output);
        $this->assertStringContainsString('Copy/inserting missing transactions to transaction table...', $output);
        $this->assertStringContainsString('END', $output);
        $this->assertStringContainsString('Total execution time in seconds:', $output);
    }

}
