<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Transaction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Transaction|null find($id, $lockMode = null, $lockVersion = null)
 * @method Transaction|null findOneBy(array $criteria, array $orderBy = null)
 * @method Transaction[]    findAll()
 * @method Transaction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @author Marcin Stanik <marcin.stanik@gmail.com>
 * @since 09.2020
 */
class TransactionRepository extends ServiceEntityRepository
{
    /**
     * TransactionRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Transaction::class);
    }

    /**
     * @param string[] $transactionIds
     * @return array
     */
    public function getArray(array $transactionIds = []): array
    {
        $qb = $this->createQueryBuilder('t');

        $query = $qb
            ->select(['t.transactionId', 'tc.name categoryName', 't.transactionInformation', 't.bookingDateTime', 't.amount', 'c.name amountCurrency'])
            ->leftJoin('t.transactionCategory', 'tc')
            ->leftJoin('t.amountCurrency', 'c');

        if (count($transactionIds) > 0) {
            $query->where($qb->expr()->in('t.transactionId', $transactionIds));
        }

        $query->orderBy('t.bookingDateTime', 'ASC');

        return array_map(
            function ($transaction) {
                $transaction['bookingDateTime'] = $transaction['bookingDateTime'] instanceof \DateTime
                    ?  $transaction['bookingDateTime']->format('Y-m-d H:i:s')
                    : '';
                return $transaction;
            },
            $query
                ->getQuery()
                ->getArrayResult()
        );
    }

    /**
     * @param int $transactionInformationGroupName
     * @param string[] $excludeTransactionIds
     * @return Transaction[]
     */
    public function getTransactionsByTransactionInformationGroupId(int $transactionInformationGroupId, array $excludeTransactionIds = []): array
    {
        $qb = $this->createQueryBuilder('t');

        $query = $qb
            ->where('t.transactionInformationGroup = :transactionInformationGroupId')
            ->setParameter(':transactionInformationGroupId', $transactionInformationGroupId);

        if (count($excludeTransactionIds) > 0) {
            $query
                ->andWhere($qb->expr()->notIn('t.transactionId', $excludeTransactionIds));
        }

        return $query
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $count
     * @param bool $fastDelete
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function deleteNewest(int $count, $fastDelete = true): void
    {
        if (!is_int($count) || $count <= 0) {
            throw new \Exception("Input parameter have to be integer and greater than 0");
        }

        if ($fastDelete) {
            $query = "
DELETE FROM transaction 
WHERE transaction_id IN (
    SELECT transaction_id 
    FROM transaction 
    ORDER BY booking_date_time DESC 
    LIMIT $count
);
        ";

            ($this->getEntityManager()->getConnection()->prepare($query))
                ->execute();
        } else {
            $query = $this->createQueryBuilder('t')
                ->orderBy('t.bookingDateTime', 'DESC')
                ->setMaxResults($count)
                ->getQuery();

            $transactionIds = [];
            /** @var Transaction $transaction */
            foreach ($query->getResult() as $transaction) {
                $transactionIds[] = $transaction->getTransactionId();
            }

            if ($transactionIds > 0) {
                $qb = $this->createQueryBuilder('t');
                $qb
                    ->delete()
                    ->where($qb->expr()->in('t.transactionId', $transactionIds))
                    ->getQuery()
                    ->execute();
            }
        }
    }

    /**
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function truncate(): void
    {
        $query = 'TRUNCATE "transaction";';

        ($this->getEntityManager()->getConnection()->prepare($query))
            ->execute();
    }

}
