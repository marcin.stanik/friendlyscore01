<?php
declare(strict_types=1);

namespace App\Tests\Command;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Class DeleteTransactionsCommandTest
 * @package App\Tests\Command
 * @author Marcin Stanik <marcin.stanik@gmail.com>
 * @since 09.2020
 */
class DeleteTransactionsCommandTest extends KernelTestCase
{
    public function testExecute()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('app:import-transactions');
        $commandTester = new CommandTester($command);
        $commandTester->execute([]);

        $this->assertEquals(Command::SUCCESS, $commandTester->getStatusCode());

        $command = $application->find('app:delete-transactions');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'recordsCount' => 10,
        ]);

        $this->assertEquals(Command::SUCCESS, $commandTester->getStatusCode());

        $command = $application->find('app:delete-transactions');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'recordsCount' => 15,
        ]);

        $this->assertEquals(Command::SUCCESS, $commandTester->getStatusCode());

        $command = $application->find('app:import-transactions');
        $commandTester = new CommandTester($command);
        $commandTester->execute([]);
    }

}
