<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transaction
 *
 * @ORM\Table(name="transaction", indexes={@ORM\Index(name="transaction_amount_currency_index", columns={"amount_currency_id"}), @ORM\Index(name="transaction_transaction_information_group_id_index", columns={"transaction_information_group_id"}), @ORM\Index(name="transaction_transaction_category_id_index", columns={"transaction_category_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\TransactionRepository")
 */
class Transaction
{
    /**
     * @var string
     *
     * @ORM\Column(name="transaction_id", type="string", length=45, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="transaction_transaction_id_seq", allocationSize=1, initialValue=1)
     */
    private $transactionId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="booking_date_time", type="datetime", nullable=true)
     */
    private $bookingDateTime;

    /**
     * @var string
     *
     * @ORM\Column(name="transaction_information", type="string", length=500)
     */
    private $transactionInformation;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=20, scale=8)
     */
    private $amount;

    /**
     * @var \Currency
     *
     * @ORM\ManyToOne(targetEntity="Currency")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="amount_currency_id", referencedColumnName="id")
     * })
     */
    private $amountCurrency;

    /**
     * @var \TransactionCategory
     *
     * @ORM\ManyToOne(targetEntity="TransactionCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="transaction_category_id", referencedColumnName="id")
     * })
     */
    private $transactionCategory;

    /**
     * @var \TransactionInformationGroup
     *
     * @ORM\ManyToOne(targetEntity="TransactionInformationGroup")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="transaction_information_group_id", referencedColumnName="id")
     * })
     */
    private $transactionInformationGroup;

    public function getTransactionId(): ?string
    {
        return $this->transactionId;
    }

    public function getBookingDateTime(): ?\DateTime
    {
        return $this->bookingDateTime;
    }

    public function setBookingDateTime(\DateTime $bookingDateTime): self
    {
        $this->bookingDateTime = $bookingDateTime;

        return $this;
    }

    public function getTransactionInformation(): ?string
    {
        return $this->transactionInformation;
    }

    public function setTransactionInformation(string $transactionInformation): self
    {
        $this->transactionInformation = $transactionInformation;

        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getAmountCurrency(): ?Currency
    {
        return $this->amountCurrency;
    }

    public function setAmountCurrency(?Currency $amountCurrency): self
    {
        $this->amountCurrency = $amountCurrency;

        return $this;
    }

    public function getTransactionCategory(): ?TransactionCategory
    {
        return $this->transactionCategory;
    }

    public function setTransactionCategory(?TransactionCategory $transactionCategory): self
    {
        $this->transactionCategory = $transactionCategory;

        return $this;
    }

    public function getTransactionInformationGroup(): ?TransactionInformationGroup
    {
        return $this->transactionInformationGroup;
    }

    public function setTransactionInformationGroup(?TransactionInformationGroup $transactionInformationGroup): self
    {
        $this->transactionInformationGroup = $transactionInformationGroup;

        return $this;
    }


}
