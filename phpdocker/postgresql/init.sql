DROP TABLE IF EXISTS "currency";
DROP SEQUENCE IF EXISTS currency_id_seq;
CREATE SEQUENCE currency_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."currency" (
    "id" integer DEFAULT nextval('currency_id_seq') NOT NULL,
    "name" character varying(10) NOT NULL,
    CONSTRAINT "currency_name_unique" UNIQUE ("name"),
    CONSTRAINT "currency_pkey" PRIMARY KEY ("id")
) WITH (oids = false);



DROP TABLE IF EXISTS "transaction_category";
DROP SEQUENCE IF EXISTS transaction_category_id_seq;
CREATE SEQUENCE transaction_category_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."transaction_category" (
    "id" integer DEFAULT nextval('transaction_category_id_seq') NOT NULL,
    "name" character varying(200) NOT NULL,
    CONSTRAINT "transaction_category_name_unique" UNIQUE ("name"),
    CONSTRAINT "transaction_category_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

COMMENT ON COLUMN "public"."transaction_category"."name" IS 'transaction category name';

TRUNCATE "transaction_category";



DROP TABLE IF EXISTS "transaction_information_group";
DROP SEQUENCE IF EXISTS transaction_information_group_id_seq;
CREATE SEQUENCE transaction_information_group_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."transaction_information_group" (
    "id" integer DEFAULT nextval('transaction_information_group_id_seq') NOT NULL,
    "name" character varying(500) NOT NULL,
    CONSTRAINT "transaction_information_group_name_unique" UNIQUE ("name"),
    CONSTRAINT "transaction_information_group_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

COMMENT ON COLUMN "public"."transaction_information_group"."name" IS 'group name';

TRUNCATE "transaction_information_group";



DROP TABLE IF EXISTS "transaction";
CREATE TABLE "public"."transaction" (
    "transaction_id" character varying(45) NOT NULL,
    "transaction_information_group_id" integer,
    "transaction_category_id" integer,
    "booking_date_time" timestamp without time zone,
    "transaction_information" character varying(500),
    "amount" numeric(20,8),
    "amount_currency_id" integer,
    CONSTRAINT "transaction_pkey" PRIMARY KEY ("transaction_id"),
    CONSTRAINT "transaction_amount_currency_id_fkey" FOREIGN KEY (amount_currency_id) REFERENCES currency(id) ON UPDATE CASCADE ON DELETE SET NULL NOT DEFERRABLE,
    CONSTRAINT "transaction_transaction_category_id_fkey" FOREIGN KEY (transaction_category_id) REFERENCES transaction_category(id) ON UPDATE CASCADE ON DELETE SET NULL NOT DEFERRABLE,
    CONSTRAINT "transaction_transaction_information_group_id_fkey" FOREIGN KEY (transaction_information_group_id) REFERENCES transaction_information_group(id) ON UPDATE CASCADE ON DELETE SET NULL NOT DEFERRABLE
) WITH (oids = false);

CREATE INDEX "transaction_amount_currency_index" ON "public"."transaction" USING btree ("amount_currency_id");

CREATE INDEX "transaction_transaction_category_id_index" ON "public"."transaction" USING btree ("transaction_category_id");

CREATE INDEX "transaction_transaction_information_group_id_index" ON "public"."transaction" USING btree ("transaction_information_group_id");

CREATE INDEX "transaction_booking_date_time_index" ON "public"."transaction" USING btree ("booking_date_time");

TRUNCATE "transaction";



INSERT INTO "public"."transaction_category"(name) VALUES ('Category 1');
INSERT INTO "public"."transaction_category"(name) VALUES ('Category 2');