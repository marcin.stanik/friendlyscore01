<?php
declare(strict_types=1);

namespace App\Command;

use App\FriendlyScore\ImportTransactions;
use SplSubject;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * php bin/console app:import-transactions -h
 * php bin/console app:import-transactions
 *
 * Class ImportTransactionsCommand
 * @package App\Command
 * @author Marcin Stanik <marcin.stanik@gmail.com>
 * @since 09.2020
 */
class ImportTransactionsCommand extends Command
    implements \SplObserver
{

    /** @var string */
    protected static $defaultName = 'app:import-transactions';

    /** @var ImportTransactions */
    protected ImportTransactions $importTransactions;

    /** @var OutputInterface */
    private OutputInterface $output;

    /**
     * ImportTransactionsCommand constructor.
     * @param ImportTransactions $importTransactions
     */
    public function __construct(ImportTransactions $importTransactions)
    {
        $this->importTransactions = $importTransactions;
        $this->importTransactions->attach($this);

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Import transactions from external API')
            ->setHelp('Script is responsible for import / add only not existing transactions from external API: ' . ImportTransactions::API_URL . ' to DB, table: transaction.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $this->output = $output;
        $result = Command::SUCCESS;

        try {
            $this->importTransactions->import();
        } catch (\Exception $ex) {
            //TODO - add information to logs
            throw $ex;

            $result = Command::FAILURE;
        }

        return $result;
    }

    /**
     * @param SplSubject $subject
     */
    public function update(SplSubject $subject): void
    {
        if($this->output instanceof OutputInterface) {
            $this->output->writeln($subject->getStatus());
        }
    }

}
