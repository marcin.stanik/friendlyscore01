<?php
declare(strict_types=1);

namespace App\Command;

use App\Service\Transaction as TransactionService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * php bin/console app:delete-transactions -h
 * php bin/console app:delete-transactions 10
 * php bin/console app:delete-transactions all
 *
 * Class ImportTransactionsCommand
 * @package App\Command
 * @author Marcin Stanik <marcin.stanik@gmail.com>
 * @since 09.2020
 */
class DeleteTransactionsCommand extends Command
{
    /** @var string  */
    const ARGUMENT_ALL = 'all';

    /** @var string */
    protected static $defaultName = 'app:delete-transactions';

    /** @var TransactionService */
    protected TransactionService $transactionService;

    public function __construct(TransactionService $transactionService)
    {
        $this->transactionService = $transactionService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Delete the newest transactions from DB')
            ->setHelp('Script is responsible for delete the newest transactions (with the newest BookingDateTime) from DB, table: transaction.')
            ->addArgument('recordsCount', InputArgument::REQUIRED, 'The amount of the newest transactions that have to be deleted. Allowed input value integer or "' . self::ARGUMENT_ALL . '"');
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $recordsCount = $input->getArgument('recordsCount');
        if (!is_numeric($recordsCount) && strtolower($recordsCount) != self::ARGUMENT_ALL) {
            throw new \Exception('Input parameter have to be integer or "' . self::ARGUMENT_ALL . '"');
        } elseif (is_numeric($recordsCount)) {
            $recordsCount = (int)$recordsCount;
        }

        $result = Command::SUCCESS;

        try {
            if (strtolower($recordsCount) == self::ARGUMENT_ALL) {
                $this->transactionService->deleteAllTransactions();
            } else {
                $this->transactionService->deleteNewestTransactions($recordsCount);
            }
        } catch (\Exception $ex) {
            //TODO - add information to logs
            throw $ex;

            $result = Command::FAILURE;
        }

        return $result;
    }

}
