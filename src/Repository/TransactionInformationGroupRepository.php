<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\TransactionInformationGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TransactionInformationGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method TransactionInformationGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method TransactionInformationGroup[]    findAll()
 * @method TransactionInformationGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @author Marcin Stanik <marcin.stanik@gmail.com>
 * @since 09.2020
 */
class TransactionInformationGroupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TransactionInformationGroup::class);
    }

    /**
     * @param string $name
     * @param bool $createIfNotExist
     * @return TransactionInformationGroup|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function getTransactionInformationGroupByName(string $name, bool $createIfNotExist = false): ?TransactionInformationGroup
    {
        static $transactionInformationGroups = [];

        if (!array_key_exists($name, $transactionInformationGroups)) {

            $transactionInformationGroups[$name] = $this->findOneBy([
                'name' => $name,
            ]);

            if ($transactionInformationGroups[$name] === null && $createIfNotExist === true) {
                $transactionInformationGroup = new TransactionInformationGroup();
                $transactionInformationGroup->setName($name);

                $this->getEntityManager()->persist($transactionInformationGroup);
                $this->getEntityManager()->flush();

                $transactionInformationGroups[$name] = $transactionInformationGroup;
            }
        }

        return $transactionInformationGroups[$name];
    }

    /**
     * @param string $name
     * @param bool $createIfNotExist
     * @return int|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function getTransactionInformationGroupIdByName(string $name, bool $createIfNotExist = false): ?int
    {
        $currency = $this->getTransactionInformationGroupByName($name, $createIfNotExist);

        return $currency !== null
            ? $currency->getId()
            : null;
    }

}
