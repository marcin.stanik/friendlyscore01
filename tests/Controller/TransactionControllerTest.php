<?php
declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class TransactionControllerTest
 *
 * php bin/phpunit tests/Controller/TransactionControllerTest
 *
 * @package App\Tests\Controller
 * @author Marcin Stanik <marcin.stanik@gmail.com>
 * @since 09.2020
 */
class TransactionControllerTest extends WebTestCase
{

    const API_BASE_URL = 'http://localhost:8000/api/';

    private static $lastTransactionId;

    private static $lastTranasctionCategory;

    public function testGet()
    {
        /*
        $client = \Symfony\Component\HttpClient\HttpClient::create();
        $response = $client->request('GET', self::API_BASE_URL . 'transaction'
        );
        $content = $response->getContent();
        */

        $client = static::createClient([]);
        $client->request('GET', 'api/transaction', []);

        $response = $client->getResponse();
        $content = $client->getResponse()->getContent();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertJson($content);

        $contentArray = json_decode($content, true);

        $this->assertIsArray($contentArray);
        $this->assertGreaterThan(0, count($contentArray));

        foreach ($contentArray as $array) {
            $this->assertArrayHasKey('transactionId', $array);
            $this->assertArrayHasKey('categoryName', $array);
            $this->assertArrayHasKey('transactionInformation', $array);
            $this->assertArrayHasKey('bookingDateTime', $array);
            $this->assertArrayHasKey('amount', $array);
            $this->assertArrayHasKey('amountCurrency', $array);
        }

        $lastTransaction = end($contentArray);

        self::$lastTransactionId = $lastTransaction['transactionId'];
        self::$lastTranasctionCategory = $lastTransaction['categoryName'];
    }

    public function testPut()
    {
        $newCategoryName = self::$lastTranasctionCategory . '_NEW';

        $client = static::createClient([]);
        $client->request('PUT', 'api/transaction/' . self::$lastTransactionId, [
            'categoryName' => $newCategoryName,
        ]);

        $response = $client->getResponse();
        $content = $client->getResponse()->getContent();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertJson($content);

        $contentArray = json_decode($content, true);

        $this->assertIsArray($contentArray);
        $this->assertGreaterThan(0, count($contentArray));

        $transactionIds = [];
        foreach ($contentArray as $array) {
            $this->assertArrayHasKey('transactionId', $array);
            $this->assertArrayHasKey('categoryName', $array);
            $this->assertArrayHasKey('transactionInformation', $array);
            $this->assertArrayHasKey('bookingDateTime', $array);
            $this->assertArrayHasKey('amount', $array);
            $this->assertArrayHasKey('amountCurrency', $array);

            $this->assertEquals($newCategoryName, $array['categoryName']);

            $transactionIds[] = $array['transactionId'];
        }

        $this->assertTrue(in_array(self::$lastTransactionId, $transactionIds));
    }

}
