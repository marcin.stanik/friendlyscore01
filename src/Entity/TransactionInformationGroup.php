<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TransactionInformationGroup
 *
 * @ORM\Table(name="transaction_information_group", uniqueConstraints={@ORM\UniqueConstraint(name="transaction_information_group_name_unique", columns={"name"})})
 * @ORM\Entity(repositoryClass="App\Repository\TransactionInformationGroupRepository")
 */
class TransactionInformationGroup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="transaction_information_group_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=500, nullable=false, options={"comment"="group name"})
     */
    private $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }


}
