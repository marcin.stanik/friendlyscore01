<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Currency;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Currency|null find($id, $lockMode = null, $lockVersion = null)
 * @method Currency|null findOneBy(array $criteria, array $orderBy = null)
 * @method Currency[]    findAll()
 * @method Currency[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @author Marcin Stanik <marcin.stanik@gmail.com>
 * @since 09.2020
 */
class CurrencyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Currency::class);
    }

    /**
     * @param string $name
     * @param bool $createIfNotExist
     * @return Currency|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function getCurrencyByName(string $name, bool $createIfNotExist = false): ?Currency
    {
        static $currencies = [];

        if (!array_key_exists($name, $currencies)) {

            $currencies[$name] = $this->findOneBy([
                'name' => $name,
            ]);

            if ($currencies[$name] === null && $createIfNotExist === true) {
                $currency = new Currency();
                $currency->setName($name);

                $this->getEntityManager()->persist($currency);
                $this->getEntityManager()->flush();

                $currencies[$name] = $currency;
            }
        }

        return $currencies[$name];
    }

    /**
     * @param string $name
     * @param bool $createIfNotExist
     * @return int|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function getCurrencyIdByName(string $name, bool $createIfNotExist = false): ?int
    {
        $currency = $this->getCurrencyByName($name, $createIfNotExist);

        return $currency !== null
            ? $currency->getId()
            : null;
    }

}
