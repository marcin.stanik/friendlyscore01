<?php
declare(strict_types=1);

namespace App\Controller;

use App\Service\Transaction as TransactionService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TransactionController
 * @package App\Controller
 * @Route("/api", name="api")
 * @author Marcin Stanik <marcin.stanik@gmail.com>
 * @since 09.2020
 */
class TransactionController extends AbstractApiController
{

    /** @var TransactionService */
    protected TransactionService $transactionService;

    public function __construct(TransactionService $transactionService)
    {
        $this->transactionService = $transactionService;
    }

    /**
     * @Route("/transaction", name="transaction", methods={"GET"})
     */
    public function getTransactions(): JsonResponse
    {
        return $this->json(
            $this->transactionService->getAllTransactionsArray()
        );
    }

    /**
     * @Route("/transaction/{transactionId}", name="transaction_put", methods={"PUT"})
     */
    public function updateTransactionCategory(string $transactionId, Request $request): JsonResponse
    {
        $categoryName = $request->request->get('categoryName');

        return $this->json(
            $this->transactionService->updateTransactionCategory($transactionId, $categoryName)
        );
    }

}
